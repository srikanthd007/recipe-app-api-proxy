# recipe app api proxy

nginx proxy app for our recipe app api

## usage

### environment variables

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of app to forward requests to (default: 'app')
* 'APP_PORT' - Port of app to forward requests to (default: '9000')

